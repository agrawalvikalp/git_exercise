﻿**Playground** 

![](Aspose.Words.3f695eb0-6f63-49e2-bae2-3a0edf39b39d.001.jpeg)

**IT IS MY HUMBLE REQUEST TO TYPE GIT COMMANDS ON THE TERMINAL RATHER THAN USING A USER INTERFACE. ALL THE EXERCISES ARE TO BE DONE IN ORDER. PLEASE DOCUMENT ALL THE COMMANDS THAT YOU ARE TYPING TO ACHIEVE THIS IN A MARKDOWN DOCUMENT FOR THE BELOW EXERCISES ALONG WITH SCREENSHOTS AND COMMIT THIS FILE IN THE REPOSITORY THAT YOU CREATE IN THE FIRST STEP BELOW AFTER THE END OF ALL THE EXERCISES. THE NAME OF THE MARKDOWN FILE SHOULD BE MY\_SUBMISSION.md.** 

**Exercises** 

- **Scenario 1:**  

  ![](Aspose.Words.3f695eb0-6f63-49e2-bae2-3a0edf39b39d.002.jpeg)

- **Scenario 2:**  

  ![](Aspose.Words.3f695eb0-6f63-49e2-bae2-3a0edf39b39d.003.jpeg)

- **Scenario 3:**  

  ![](Aspose.Words.3f695eb0-6f63-49e2-bae2-3a0edf39b39d.004.jpeg)

  ![](Aspose.Words.3f695eb0-6f63-49e2-bae2-3a0edf39b39d.005.jpeg)

  Merge request  

- **Scenario 4:**  

![](Aspose.Words.3f695eb0-6f63-49e2-bae2-3a0edf39b39d.006.jpeg)

I did the same in command window with  

git diff main  

but I forgot to take the screen shot 

so I made it for a similar scenario 

![](Aspose.Words.3f695eb0-6f63-49e2-bae2-3a0edf39b39d.007.jpeg)

- **Scenario 6:**  

![](Aspose.Words.3f695eb0-6f63-49e2-bae2-3a0edf39b39d.008.jpeg)

- **Scenario 7:**  

  ![](Aspose.Words.3f695eb0-6f63-49e2-bae2-3a0edf39b39d.009.jpeg)

- **Scenario 8:**  

  Was not able to this  Showed error  

remote: You are not allowed to push code to this project. 

fatal: unable to access 'https://gitlab.com/agrawalvikalp/git\_exercise\_copy.git/': The requested URL returned error: 403 

Tried to unprotect the branch but still didn’t work  

- **Scenario 9:**  

![](Aspose.Words.3f695eb0-6f63-49e2-bae2-3a0edf39b39d.010.jpeg)

Add the document file with all your reasons, commands and snapshots and commit it in the repository. 
